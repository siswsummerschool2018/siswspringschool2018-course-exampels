#pragma once

#include <qobject.h>
#include <qdial.h>
#include <QDoubleSpinBox>
#include "mainwindow.h"

class ValueManager : public QObject
{
	Q_OBJECT

public:
	ValueManager(QDial * majorValueDial, QDial * minorValueDial, QDoubleSpinBox * doubleValueInput);

signals:
	void valueChanged(double newValue);

public slots:
	void updateMajorDial(double newValue);
	void updateMinorDial(double newValue);
	void updateInputMajorValue(int majorValue);
	void updateInputMinorValue(int minorValue);

private:
	QDial * mMajorValueDial;
	QDial * mMinorValueDial;
	QDoubleSpinBox * mDoubleValueInput;

	int getMajorValue(double value);
	int getMinorValue(double value);
	double buildValue(int majorVlaue, int minorValue);
};
