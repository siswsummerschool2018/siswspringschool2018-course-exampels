#include <QApplication>

#include <iostream>
#include "mainwindow.h"
#include "ValueManager.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	MainWindow mainWindow;
	mainWindow.show();

	ValueManager * valueManager = new ValueManager(
		mainWindow.getMajorValueDial(),
		mainWindow.getMinorValueDial(),
		mainWindow.getDoubleValueInput()
	);

	return app.exec();
}
