#include "ValueManager.h"

ValueManager::ValueManager(QDial * majorValueDial, QDial * minorValueDial, QDoubleSpinBox * doubleValueInput) :
	mMajorValueDial(majorValueDial),
	mMinorValueDial(minorValueDial),
	mDoubleValueInput(doubleValueInput)
{
	connect(mDoubleValueInput, SIGNAL(valueChanged(double)),
		this, SLOT(updateMajorDial(double)));
	connect(mDoubleValueInput, SIGNAL(valueChanged(double)),
		this, SLOT(updateMinorDial(double)));
	connect(mMajorValueDial, SIGNAL(valueChanged(int)),
		this, SLOT(updateInputMajorValue(int)));
	connect(mMinorValueDial, SIGNAL(valueChanged(int)),
		this, SLOT(updateInputMinorValue(int)));
}

void ValueManager::updateMajorDial(double newValue)
{
	mMajorValueDial->setValue(getMajorValue(newValue));
}

int ValueManager::getMajorValue(double value)
{
	return (int)value;
}

void ValueManager::updateMinorDial(double newValue)
{
	mMinorValueDial->setValue(getMinorValue(newValue));
}

int ValueManager::getMinorValue(double value)
{
	return (int)((value - (int)value) * 100);
}

double ValueManager::buildValue(int majorValue, int minorValue)
{
	double minorValueDec = (double)minorValue / 100;
	return majorValue + minorValueDec;
}

void ValueManager::updateInputMajorValue(int majorValue)
{
	int minorValue = mMinorValueDial->value();
	mDoubleValueInput->setValue(buildValue(majorValue, minorValue));
}

void ValueManager::updateInputMinorValue(int minorValue)
{
	int majorValue = mMajorValueDial->value();
	mDoubleValueInput->setValue(buildValue(majorValue, minorValue));
}
