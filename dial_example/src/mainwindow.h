#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <qdial.h>
#include <QDoubleSpinBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
	
	inline QDial * getMajorValueDial() {
		return ui->majorValueDial;
	}

	inline QDial * getMinorValueDial() {
		return ui->minorValueDial;
	}

	inline QDoubleSpinBox * getDoubleValueInput() {
		return ui->doubleValueInput;
	}

    ~MainWindow();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
