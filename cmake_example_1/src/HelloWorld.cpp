#include <iostream>

int main(int argc, char ** argv)
{
	std::cout << "Hello world!" << std::endl;
	for (int i = 1 /* exclude filename */; i < argc; i++)
	{
		std::cout << "Argument #" << i << ": " << argv[i] << std::endl;
	}
	std::cin.get();
	return 0;
}
