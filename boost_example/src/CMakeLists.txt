# Defines the minimum CMake version required for the CMakeLists.txt file
# to be correctly interpreted. Older versions of CMake may not contain
# all the features to "understand" this file.
cmake_minimum_required(VERSION 3.10.0 FATAL_ERROR)

# Defines the name and language the project will be using.
project(HelloBoost LANGUAGES CXX)

# Instruct CMake to take "libraries" subfolder as a valid source for
# include lookup.
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/libraries)

# Automatically add the current source and build directories to the
# include path.
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Add to PROJECT_SOURCES variable all the filenames inside
# ${CMAKE_CURRENT_SOURCE_DIR} which respect the given pattern.
file(GLOB PROJECT_SOURCES "*.cpp")

# Instruct CMake to create an executable based on all the .cpp
# sources of the project. You may, for example, create multiple
# executables based on different source files, inside the same
# project.
add_executable(${PROJECT_NAME} ${PROJECT_SOURCES})
